import Vue from 'vue';
import Router from 'vue-router';
import LandingPage from '@/views/LandingPage';
import AppDashboard from '@/views/AppDashboard';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'landing-page',
      component: LandingPage
    },
    {
      path: '/app',
      name: 'AppDashboard',
      component: AppDashboard
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
