import { exec } from 'child_process';


let getCommits = (dir) => {
  return new Promise((resolve, reject) => {

    exec(`git --git-dir ${dir}\\.git log --pretty=oneline --author-date-order`, (err, stdout, stderr) => {

      if (err) return reject(err);
      if(stderr) return reject(stderr);

      resolve(stdout.split('\n'));
      
    })
  });
}

let getStats = (dir) => {
  
}

let Git = {
  getCommits
};

export { Git };